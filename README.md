# RollAndDodge

RollAndDodge est un jeu mobile développé par Marc THIERY et Noan RANDON. 

Lorsque l'utilisateur ouvre l'application pour la première fois, il est invité à créer un compte ou à se connecter. Cette étape est gérée par une base de données, si l'utilisateur se connecte une fois, il restera connecté jusqu'à ce qu'il clique sur le bouton "Déconnexion" dans les paramètres."

Après s'être crée un compte/connecté, on arrive sur le menu du jeu ou l'on retrouve plusieurs boutons ayant tous un rôle différent. A noter que la page menu gère le mode portrait et paysage.  

Pour commencer à jouer, il suffit de cliquer sur le bouton "Start a Party". Le but du jeu est de contrôler une balle à l'aide du gyroscope et d'éviter des obstacles. Les obstacles sont générés de plus en plus rapidement, offrant une évolution de la difficulté au fil de la partie. Le joueur dispose de 3 vies pour réaliser le meilleur score, qui est représenté par le temps de survie du joueur. S'il le souhaite le joueur à la possibilité de mettre en pause sa partie ou de la quitter.

Le bouton "Settings" permet de modifier le jeu. Pour le moment, il est possible de changer le thème du jeu (jour/nuit). Mais aussi de se déconnecter.

Le bouton "Information" dirige l'utilisateur vers une page présentant la liste des joueurs avec leur meilleur score, qui est affichée sous forme d'une recyclerView. De plus, il y a un bouton "Règles" qui conduit à une page expliquant les règles du jeu. Ces règles sont stockées dans un dépôt Git, que l'application récupère à distance.

Enfin, un bouton "Quit" permet de quitter l'application.

# Bug connu

Si lors de la création du compte / connexion au compte, l'application crash alors :
 * Allez dans les paramètres de votre téléphone
 * Cliquez sur l'onglet "Application"
 * Trouvez l'application "Roll&Dodge"
 * Cherchez un onglet "Storage" / "Stockage"
 * Supprimez les données / caches  
 * Relancez l'application
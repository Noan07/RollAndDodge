package adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import entity.Player
import uca.iut.clermont.R

class PlayerListAdapter(private val players: List<Player>) :
    RecyclerView.Adapter<PlayerListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_player, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(players[position])
    }

    override fun getItemCount(): Int {
        return players.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val nameTextView: TextView = view.findViewById(R.id.player_name)
        private val scoreTextView: TextView = view.findViewById(R.id.player_score)

        fun bind(player: Player) {
            nameTextView.text = player.name
            scoreTextView.text = player.score.toString()
        }
    }
}


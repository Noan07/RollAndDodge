package model

class Game {
    private var timeInSeconds: Long = 0
    private var lives = 3
    private val ball = Ball()

    fun setTimeInSeconds(time: Long) {
        timeInSeconds = time
    }

    fun getTimeInSeconds(): Long {
        return timeInSeconds
    }

    fun updateTime() {
        timeInSeconds++
    }

    fun getBall(): Ball {
        return ball
    }

    fun startNewGame(ballSpeed: Float) {
        timeInSeconds = 0
        lives = 3
        ball.setSpeed(ballSpeed)
    }

    fun reduceLife() {
        lives--
    }

    fun getLives(): Int {
        return lives
    }

}
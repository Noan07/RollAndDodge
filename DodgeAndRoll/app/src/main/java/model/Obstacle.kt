package model

import android.widget.ImageView
import kotlinx.coroutines.*
import java.util.*

class Obstacle(val width: Float, val height: Float, val _widthScreen: Float, val heightScreen: Float, val time: Float,val image : ImageView) {
    val startX: Float
    var startY: Float
    val endX: Float
    val speed: Float

    init {
        val random = Random()
        startX = random.nextFloat() * (_widthScreen - width)
        startY = 0f
        endX = heightScreen
        speed = heightScreen / (time / 1000f) // calculate speed needed to reach endX in time
    }



    private var job: Job? = null

    suspend fun startMovement() = coroutineScope {
        job = launch {
            while (startY < endX) {
                image.y=startY
                startY += speed * DELTA_TIME
                delay(16)
            }
        }
        job?.join()
    }

    fun pauseMovement() {
        job?.cancel()
    }

    fun resumeMovement() {
        job = null
        GlobalScope.launch(Dispatchers.Main) { startMovement() }
    }

    companion object {
        private const val DELTA_TIME = 0.016f // 60fps
    }
}
package model

class Ball {
    private var x: Float = 0f
    private var y: Float = 0f
    private var speed: Float = 0f

    fun setX(x: Float) {
        this.x = x
    }

    fun setY(y: Float) {
        this.y = y
    }

    fun getX(): Float {
        return x
    }

    fun getY(): Float {
        return y
    }

    fun setSpeed(speed: Float) {
        this.speed = speed
    }
}
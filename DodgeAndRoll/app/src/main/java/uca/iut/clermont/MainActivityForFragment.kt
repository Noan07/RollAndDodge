package uca.iut.clermont

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle
import fragment.ConnexionFragment
import fragment.MainFragment
import fragment.MainFragmentLandscape

class MainActivityForFragment : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_for_fragment)
        if(savedInstanceState == null)
        {
            supportFragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.Fragmentcontainer, ConnexionFragment())
                .commit()
        }

    }
}
package fragment

import android.app.AlertDialog
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Resources
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.lifecycleScope
import database.AppDatabase
import kotlinx.coroutines.*
import model.Game
import model.Obstacle
import uca.iut.clermont.R
import java.util.*

class PlayFragment : Fragment(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var gyroscope: Sensor? = null
    private var isPaused = false
    private var gameObstacle = true
    private lateinit var game: Game
    private lateinit var timeTextView: TextView
    private lateinit var ballView: View
    private var timerJob: Job? = null
    private var timeScope: CoroutineScope? = null
    private var isDialogShowing = false
    lateinit var random: Random
    lateinit var linearLayout: RelativeLayout
    private val scope = CoroutineScope(Dispatchers.Main)
    private val obstacles = mutableListOf<Obstacle>()
    private lateinit var pauseButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        return inflater.inflate(R.layout.fragment_play, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sensorManager = requireContext().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        ballView = view.findViewById(R.id.ballView)

        game = Game()

        val ballView = view.findViewById<View>(R.id.ballView)
        val screenCenterX = getScreenWidth() / 2
        val screenCenterY = getScreenHeight() / 2

        game.getBall().setX(screenCenterY)
        game.getBall().setY(screenCenterX)

        pauseButton = view.findViewById(R.id.pauseButton)
        pauseButton.setOnClickListener { onPauseButtonClick() }


        timeTextView = view.findViewById(R.id.timerTextView)
        timeTextView.text = "0"
        startParty()
    }

    private fun getScreenWidth() = Resources.getSystem().displayMetrics.widthPixels.toFloat()
    private fun getScreenHeight() = Resources.getSystem().displayMetrics.heightPixels.toFloat()

    private fun startParty() {
        random = Random()
        linearLayout = view?.findViewById(R.id.showObs)!!
        startScope()
    }

    private fun startScope() {
        var initDelay = 1000L
        scope.launch {
            while (gameObstacle) {
                addObstacle()
                delay(initDelay)
                if(initDelay > 200L) {
                    initDelay -= 8
                }
                if (isPaused) {
                    obstacles.forEach { it.pauseMovement() }
                    while (isPaused) {
                        delay(100)
                    }
                    obstacles.forEach { it.resumeMovement() }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        gyroscope?.let {
            sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_GAME)
        }
        if (!isDialogShowing) {

            if (!isPaused) {
                startTimer()
                obstacles.forEach { it.resumeMovement() }
            }
        }
    }
    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
        pauseTimer()
        saveTime()
    }

    private fun saveTime() {
        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putLong("timeInSeconds", game.getTimeInSeconds()).apply()
    }


    private fun pauseTimer() {
        timerJob?.cancel()
        timeScope?.cancel()
    }


    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_GYROSCOPE) {
            var x = game.getBall().getX()
            var y = game.getBall().getY()
            x += event.values[0] * 50f
            y += event.values[1] * 50f

            if (y < 0) y = 0f
            if (y + ballView.width > getScreenWidth()) y = getScreenWidth() - ballView.width
            if (x < 0) x = 0f
            if (x + ballView.height > getScreenHeight()) x = getScreenHeight() - ballView.height
            game.getBall().setX(x)
            game.getBall().setY(y)
            ballView.x = y
            ballView.y = x
            checkCollision()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    private fun startTimer() {
        if (!isDialogShowing) {
            timerJob = Job()
            timeScope = CoroutineScope(Dispatchers.Main + timerJob!!)
            timeScope?.launch {
                while (true) {
                    delay(1000)
                    game.updateTime()
                    updateTime()
                }
            }
        }
    }

    private fun stopTimer() {
        timerJob?.cancel()
        timeScope?.cancel()
    }



    private fun onPauseButtonClick() {
        saveTime()
        isPaused = true
        pauseTimer()
        obstacles.forEach { it.pauseMovement() }
        showPauseDialog()
    }

    private fun showPauseDialog() {
        isDialogShowing = true
        val dialogView = layoutInflater.inflate(R.layout.dialog_pause, null)
        val continueButton = dialogView.findViewById<Button>(R.id.continueButton)
        val quitButton = dialogView.findViewById<Button>(R.id.quitButton)

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        continueButton.setOnClickListener {
            dialog.dismiss()
            loadTime()

            obstacles.forEach { it.resumeMovement() }
            isPaused = false
            isDialogShowing = false
            obstacles.clear()
            startTimer()
        }

        quitButton.setOnClickListener {
            dialog.dismiss()
            val mainFragment = MainFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, mainFragment)
                .commit()
        }
    }

    private fun updateTime() {
        timeTextView.text = game.getTimeInSeconds().toString()
    }

    private fun checkCollision() {
        for (obstacle in obstacles) {
            val obstacleX = obstacle.startX
            val obstacleY = obstacle.startY

            if (ballView.x >= obstacleX - (obstacle.image.width / 2) && ballView.x <= obstacleX + (obstacle.image.width / 2) && ballView.y >= obstacleY - (obstacle.image.height / 2) && ballView.y <= obstacleY + (obstacle.height / 9)) {
                val lives = game.getLives()
                game.reduceLife()
                game.getBall().setY(getScreenWidth() / 2)
                game.getBall().setX(getScreenHeight() / 2)
                for (obstacle in obstacles) {
                    linearLayout.removeView(obstacle.image)
                }
                obstacles.clear()
                if (lives == 3) {
                    val imageView = view?.findViewById<ImageView>(R.id.heart3)
                    imageView?.setImageResource(R.drawable.heart_empty)
                } else if (lives == 2) {
                    val imageView = view?.findViewById<ImageView>(R.id.heart2)
                    imageView?.setImageResource(R.drawable.heart_empty)
                } else {
                    val imageView = view?.findViewById<ImageView>(R.id.heart1)
                    imageView?.setImageResource(R.drawable.heart_empty)
                    gameOver()
                }
                break
            }
        }
    }


    private suspend fun addObstacle() {
        if (!isAdded) { // isAdded() pour vérifier si le fragment est attaché à l'activité
            return
        }

        //linearLayout.addView(imageView)
        val obstacleWidth = 300f
        val obstacleHeight = 200f
        val screenHeight = Resources.getSystem().displayMetrics.heightPixels.toFloat()
        val screenWidth = Resources.getSystem().displayMetrics.widthPixels.toFloat()
        val duration = 3000f

        val imageView = ImageView(requireContext())

        imageView.setImageResource(R.drawable.barre)
        imageView.layoutParams = RelativeLayout.LayoutParams(obstacleWidth.toInt(), obstacleHeight.toInt())
        val obstacle = Obstacle(obstacleWidth, obstacleHeight, screenWidth, screenHeight, duration, imageView)
        obstacles.add(obstacle)
        imageView.x = obstacle.startX
        imageView.y = obstacle.startY


        linearLayout.addView(imageView)
        CoroutineScope(Dispatchers.Main).launch {
            obstacle.startMovement()

            withContext(Dispatchers.Main) {
                linearLayout.removeView(imageView)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        gameObstacle = false
        obstacles.clear()
    }

    private fun gameOver() {
        val sharedPreferences = requireActivity().getSharedPreferences("username", Context.MODE_PRIVATE)
        val username = sharedPreferences.getString("isLoggedIn", "")
        val dao = AppDatabase.getInstance(requireContext()).PlayerDao()
        val toastText = "You survive : ${game.getTimeInSeconds()} secondes"
        Toast.makeText(requireContext(), toastText, Toast.LENGTH_SHORT).show()
        lifecycleScope.launch {
            username?.let { name ->
                val player = dao.getPlayersByName(name)
                if(player.score < game.getTimeInSeconds().toInt()) {
                    dao.updateScore(name, game.getTimeInSeconds().toInt())
                }
            }
            stopTimer()
            onDestroy()
            val mainFragment = MainFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, mainFragment)
                .commit()
        }
    }



    private fun loadTime() {
        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val timeInSeconds = sharedPreferences.getLong("timeInSeconds", 0)
        game.setTimeInSeconds(timeInSeconds)
        updateTime()
    }
}

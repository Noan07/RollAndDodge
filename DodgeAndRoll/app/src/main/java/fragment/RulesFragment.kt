package fragment

import android.content.pm.ActivityInfo
import android.widget.TextView
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import okhttp3.*
import uca.iut.clermont.R
import java.io.IOException

class RulesFragment : Fragment() {

    private lateinit var rulesTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        return inflater.inflate(R.layout.fragment_rules, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backButton = requireView().findViewById<Button>(R.id.buttonBack)

        backButton.setOnClickListener {
            val playFragment = PlayerListFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, playFragment)
                .commit()
        }

        rulesTextView = view.findViewById(R.id.RulesTextView)

        val client = OkHttpClient()
        val url = "https://raw.githubusercontent.com/Marcolecho/Rules/main/information"

        val request: Request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                activity?.runOnUiThread {   // runOnUiThread() pour mettre à jour l'interface utilisateur depuis le thread principal.
                    rulesTextView.text = "Error while getting rules"
                }
            }

            override fun onResponse(call: Call, response: Response) {
                if(response.isSuccessful){
                    val responseBody = response.body?.string()
                    activity?.runOnUiThread {
                        rulesTextView.text = responseBody.toString()
                    }
                }
                else{
                    activity?.runOnUiThread {
                        rulesTextView.text = "Error while getting rules"
                    }
                }
            }
        })
    }
}
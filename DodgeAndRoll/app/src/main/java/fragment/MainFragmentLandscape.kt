package fragment

import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import uca.iut.clermont.R
import kotlin.system.exitProcess

class MainFragmentLandscape : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_landscape, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val buttonLaunch = view.findViewById<Button>(R.id.buttonLaunch)
        val buttonSettings = view.findViewById<Button>(R.id.buttonSettings)
        val buttonRules = view.findViewById<Button>(R.id.buttonRules)
        val buttonExit = view.findViewById<Button>(R.id.buttonExit)

        buttonLaunch.setOnClickListener {
            val playFragment = PlayFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, playFragment)
                .commit()
        }

        buttonSettings.setOnClickListener {
            val settingsFragment = SettingsFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, settingsFragment)
                .commit()
        }

        buttonRules.setOnClickListener {
            val playersFragment = PlayerListFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, playersFragment)
                .commit()
        }
        buttonExit.setOnClickListener {
            exitProcess(1)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        val fragmentManager = requireActivity().supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            fragmentTransaction.replace(R.id.Fragmentcontainer, MainFragment())
        }

        fragmentTransaction.commit()
    }
}
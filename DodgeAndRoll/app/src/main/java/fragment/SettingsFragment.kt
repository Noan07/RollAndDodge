package fragment

import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import androidx.appcompat.app.AppCompatDelegate
import uca.iut.clermont.R


class SettingsFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backButton = requireView().findViewById<Button>(R.id.buttonBack)
        val disconnectButton = requireView().findViewById<Button>(R.id.disconnect_button)

        backButton.setOnClickListener {
            val mainFragment = MainFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, mainFragment)
                .commit()
        }
        disconnectButton.setOnClickListener {
            val sharedPreferences =
                requireActivity().getSharedPreferences("myPrefs", Context.MODE_PRIVATE)
            sharedPreferences.edit().putBoolean("isLoggedIn", false).apply()
            val connexionFragment = ConnexionFragment()
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, connexionFragment)
                .commit()
        }

        val rootView = view.rootView

        val btnModeNuit = requireView().findViewById<Switch>(R.id.switchMode)

        btnModeNuit.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                btnModeNuit.setTextColor(Color.WHITE)
                rootView.setBackgroundColor(Color.BLACK)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                btnModeNuit.setTextColor(Color.BLACK)
                rootView.setBackgroundColor(Color.parseColor("#f4b41a"))
            }
        }

        val isNightMode : Boolean = AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
        btnModeNuit.isChecked = isNightMode
    }
}
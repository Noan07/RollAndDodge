package fragment

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.lifecycleScope
import database.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uca.iut.clermont.R


class ConnexionFragment : Fragment() {
    private lateinit var usernameEditText: EditText
    private lateinit var passwordEditText: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        return inflater.inflate(R.layout.fragment_connexion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences =
            requireActivity().getSharedPreferences("myPrefs", Context.MODE_PRIVATE)
        val isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false)
        if (isLoggedIn) {
            val mainFragment = MainFragment()
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, mainFragment)
                .commit()
        } else {
            usernameEditText = view.findViewById(R.id.username_edit_text)
            passwordEditText = view.findViewById(R.id.password_edit_text)

            val loginButton = view.findViewById<Button>(R.id.login_button)
            loginButton.setOnClickListener {
                val username = usernameEditText.text.toString()
                val password = passwordEditText.text.toString()

                if (username.isBlank() || password.isBlank()) {
                    Toast.makeText(
                        requireContext(),
                        "Username and password are required",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val dao = AppDatabase.getInstance(requireContext()).PlayerDao()
                    viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                        val player = withContext(Dispatchers.IO) {
                            dao.findByNameAndPassword(
                                username,
                                password
                            )
                        }
                        if (player != null) {
                            val sharedPreferences =
                                requireActivity().getSharedPreferences("myPrefs", Context.MODE_PRIVATE)
                            sharedPreferences.edit().putBoolean("isLoggedIn", true).apply()
                            val sharedPreferencesName =
                                requireActivity().getSharedPreferences("username", Context.MODE_PRIVATE)
                            sharedPreferencesName.edit().putString("isLoggedIn", username).apply()



                            val mainFragment = MainFragment()
                            requireActivity().supportFragmentManager.beginTransaction()
                                .replace(R.id.Fragmentcontainer, mainFragment)
                                .commit()
                        } else {
                            // Login failed, show error message
                            Toast.makeText(
                                requireContext(),
                                "Invalid username or password",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }

            val registerButton = view.findViewById<Button>(R.id.register_button)
            registerButton.setOnClickListener {
                val inscriptionFragment = InscriptionFragment()
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.Fragmentcontainer, inscriptionFragment)
                    .commit()
            }
        }
    }
}

package fragment

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.lifecycleScope
import dao.DaoPlayer
import database.AppDatabase
import entity.Player
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import uca.iut.clermont.R


class InscriptionFragment : Fragment() {
    private lateinit var nameEditText: EditText
    private lateinit var passwordEditTextSecond: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var registerButton: Button
    private lateinit var loginButton: Button

    private lateinit var dao: DaoPlayer
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        return inflater.inflate(R.layout.fragment_inscription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameEditText = view.findViewById(R.id.nameEditText)
        passwordEditText = view.findViewById(R.id.editTextPassword)
        passwordEditTextSecond = view.findViewById(R.id.editTextConfirmPassword)
        registerButton = view.findViewById(R.id.registerButton)
        loginButton = view.findViewById(R.id.login_button)

        dao = AppDatabase.getInstance(requireContext()).PlayerDao()

        registerButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val password = passwordEditText.text.toString()
            val password_conf = passwordEditTextSecond.text.toString()

            if (name.isNotEmpty() && password_conf.isNotEmpty() && password.isNotEmpty() && password == password_conf) {
                lifecycleScope.launch {
                    val existingPlayer = withContext(Dispatchers.IO) {
                        dao.getPlayersByName(name)
                    }
                    if (existingPlayer != null) {
                        Toast.makeText(requireContext(), "Un joueur avec ce nom existe déjà", Toast.LENGTH_SHORT).show()
                    } else {
                        val player = Player(uid = 0, name = name, password = password, score = 0)
                        withContext(Dispatchers.IO) {
                            dao.insertAll(player)
                        }
                        Toast.makeText(requireContext(), "Inscription réussie", Toast.LENGTH_SHORT).show()
                        val connexionFragment = ConnexionFragment()
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.Fragmentcontainer, connexionFragment)
                            .commit()
                    }
                }
            } else {
                Toast.makeText(requireContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show()
            }

        }
        loginButton.setOnClickListener {
            val connexionFragment = ConnexionFragment()
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, connexionFragment)
                .commit()
        }
    }
}
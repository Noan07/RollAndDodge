package fragment

import adapter.PlayerListAdapter
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import database.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import uca.iut.clermont.R
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch

class PlayerListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: PlayerListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_player_list, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        adapter = PlayerListAdapter(emptyList())
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dao = AppDatabase.getInstance(requireContext()).PlayerDao()
        viewLifecycleOwner.lifecycleScope.launch {
            val players = withContext(Dispatchers.IO) { dao.getAllPlayers() }
            adapter = PlayerListAdapter(players)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
        }

        val rulesButton = requireView().findViewById<Button>(R.id.buttonRules)
        rulesButton.setOnClickListener {
            val rulesFragment = RulesFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, rulesFragment)
                .commit()
        }


        val backButton = requireView().findViewById<Button>(R.id.buttonBack)

        backButton.setOnClickListener {
            val mainFragment = MainFragment()
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.Fragmentcontainer, mainFragment)
                .commit()
        }

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }
}




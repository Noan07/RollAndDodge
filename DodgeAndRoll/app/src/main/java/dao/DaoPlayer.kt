package dao

import entity.Player
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DaoPlayer {
    @Query("SELECT * FROM Player WHERE name = :name AND password = :password")
    suspend fun findByNameAndPassword(name: String, password: String): Player

    @Query("SELECT * FROM Player WHERE name = :name")
    suspend fun getPlayersByName(name: String): Player

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(player: Player)

    @Query("SELECT * FROM Player")
    suspend fun getAllPlayers(): List<Player>

    @Query("UPDATE Player SET score = :score WHERE name = :name")
    suspend fun updateScore(name: String, score: Int)
}